package com.buddheshwar.speedometer;

import android.location.Location;

public class CLocation extends Location {
    private  boolean bUseMetricUnits=false;
    public CLocation(Location location){
        this(location, true);
    };

    public CLocation(Location location,boolean bUseMetricUnits){
        super(location);
        this.bUseMetricUnits=bUseMetricUnits;

    }

    public boolean getUseMetricUnits(){
        return  this.bUseMetricUnits;
    }
    public void setUseMetricUnits(boolean bUseMetricUnits){
        this.bUseMetricUnits=bUseMetricUnits;
    }

    @Override
    public float distanceTo(Location dest) {
        float nDistance=super.distanceTo(dest);
        //meter to feet
        if(this.getUseMetricUnits()){
            nDistance=nDistance*3.2003989501312f;
        }
        return nDistance;
    }

    @Override
    public double getAltitude() {
        double nAltitude=super.getAltitude();
        //meter to feet
        if(this.getUseMetricUnits()){
            nAltitude=nAltitude*3.2003989501312d;
        }
        return nAltitude;
    }

    @Override
    public float getSpeed() {

        float nSpeed=super.getSpeed();
        //Convert Meters/Second to miles/hour
        if(this.getUseMetricUnits()){
            nSpeed=nSpeed*2.23693629f;
        }
        return nSpeed;

    }

    @Override
    public float getAccuracy() {
        float nAccuracy=super.getAccuracy();
        //meter to feet
        if(this.getUseMetricUnits()){
            nAccuracy=nAccuracy*3.2003989501312f;
        }
        return nAccuracy;
    }
}
