package com.buddheshwar.speedometer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.ntt.customgaugeview.library.GaugeView;

import java.util.Formatter;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {
    GaugeView gauge;

    SwitchCompat switchCompat_speed;
    TextView tv_speed,tv_message;
    float val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switchCompat_speed = findViewById(R.id.switch1);
        tv_speed = findViewById(R.id.textView);
        gauge=(GaugeView) findViewById(R.id.gv_meter);
        tv_message=findViewById(R.id.tv_message);
        val=0;
        gauge.setTargetValue(0);


        LocationManager lm=(LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled;


        try {

            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if(!gps_enabled){
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Location disabled")  // GPS not found
                        .setMessage("Want to enable?") // Want to enable?
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
            else{
                tv_message.setVisibility(View.GONE);
            }


        }
        catch (Exception e){

        }


        //check for gps permission
        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1000);

        } else {
            //start the program
            doStuffs();
        }

        this.updateSpeed(null);
        switchCompat_speed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MainActivity.this.updateSpeed(null);
            }
        });


    }

    @SuppressLint("MissingPermission")
    private void doStuffs() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        }
       // Toast.makeText(getApplicationContext(),"Waiting for GPS Connection",Toast.LENGTH_SHORT).show();
    }

    private  void updateSpeed(CLocation cLocation){

        float nCurrentSpeed=0;
        if(cLocation!=null){
            cLocation.setUseMetricUnits(this.useMatrixUnits());
            nCurrentSpeed=cLocation.getSpeed();
            gauge.setTargetValue(nCurrentSpeed);

        }

        Formatter fmt=new Formatter(new StringBuilder());
        fmt.format(Locale.US,"%5.1f",nCurrentSpeed);
        String strCurrentSpeed=fmt.toString();
        strCurrentSpeed=strCurrentSpeed.replace(" ","0");


        if(this.useMatrixUnits()){
            tv_speed.setText(strCurrentSpeed+"km/h");
        }
        else
            tv_speed.setText(strCurrentSpeed+"miles/h");

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==1000){
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                doStuffs();

            }
            else{
                finish();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if(location!=null){
            CLocation mylocation=new CLocation(location,this.useMatrixUnits());
            this.updateSpeed(mylocation);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        tv_message.setVisibility(View.GONE);

    }

    @Override
    public void onProviderDisabled(String provider) {

        tv_message.setVisibility(View.VISIBLE);


    }

    private boolean useMatrixUnits() {
        return switchCompat_speed.isChecked();
    }




}